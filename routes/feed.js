// Load Module Dependencies
var express = require('express');
var multer  = require('multer');
var path  = require('path');

var feedController = require('../controllers/feed');




var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './public/images');
	},
	filename: function (req, file, cb){
		var ext = path.extname(file.originalname);
		cb(null, Date.now()+ext);
	}
});

var router = express.Router();
var upload = multer({storage: storage});


router.post('/volunteer/:id/postFeed', upload.single('picture'), feedController.postFeedVolunteer);

router.post('/charity/:id/postFeed', upload.single('picture'), feedController.postFeedCharity);

router.get('/:id/getTimelineVolunteer', feedController.getTimeLineVolunteer);

router.get('/:id/getUserFeed', feedController.getUserFeed);

router.delete('/:id', feedController.deleteFeed);

router.put('/:id', feedController.updateFeed);

router.post('/:id/like', feedController.like);

router.post('/:id/unlike', feedController.Unlike);

router.post('/:id/comment', feedController.comment);



// Expose Router Interface
module.exports = router;