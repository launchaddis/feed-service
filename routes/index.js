// Load Module Dependencies

var feedRouter = require('./feed');


module.exports = function appRouter(app) {
	// Users Routes
	app.use('/feeds', feedRouter);

	app.get('/get/home', function getRoot(req, res){
		res.json({
			message: 'evolunt-auth API running'
		})
	});

}