// Access Layer for Feed Data.

/**
 * Load Module Dependencies.
 */
var debug   = require('debug')('api:dal-Feed');
var moment  = require('moment');
var _       = require('lodash');

var Feed        = require('../models/feed');
var User        = require('../models/user');
var Charity     = require('../models/charity');
var Volunteer   = require('../models/volunteer');
var Event       = require('../models/event');
var Fund        = require('../models/fund');

var returnFields     = Feed.whitelist;
var population = [{
  path: 'volunteer',
  select: Volunteer.whitelistFeed
},{
  path: 'charity',
  select: Charity.whitelistFeed
},{
  path: 'event',
  select: Event.whitelistFeed
},{
  path: 'fund',
  select: Fund.whitelistFeed
},{
  path: 'user',
  select: User.whitelistFeed
},{
  path: 'comments.volunteer',
  model: 'Volunteer',
  select: Volunteer.whitelistFeed
},{
  path: 'comments.charity',
  model: 'Charity',
  select: Charity.whitelistFeed
}
];

/**
 * create a new Feed.
 *
 * @desc  creates a new Feed and saves them
 *        in the database
 *
 * @param {Object}  FeedData  Data for the Feed to create
 * @param {Function} cb       Callback for once saving is complete
 */
exports.create = function create(FeedData, cb) {
  debug('creating a new Feed');

    // Create Feed if is new.
    var FeedModel  = new Feed(FeedData);

    FeedModel.save(function saveFeed(err, data) {
      if (err) {
        return cb(err);
      }


      exports.get({ _id: data._id }, function (err, Feed) {
        if(err) {
          return cb(err);
        }

        cb(null, Feed);

      });

    });


};

/**
 * delete a Feed
 *
 * @desc  delete data of the Feed with the given
 *        id
 *
 * @param {Object}  query   Query Object
 * @param {Function} cb Callback for once delete is complete
 */
exports.delete = function deleteItem(query, cb) {
  debug('deleting Feed: ', query);

  Feed
    .findOne(query, returnFields)
    .populate(population)
    .exec(function deleteFeed(err, Feed) {
      if (err) {
        return cb(err);
      }

      if(!Feed) {
        return cb(null, {});
      }

      Feed.remove(function(err) {
        if(err) {
          return cb(err);
        }

        cb(null, Feed);

      });

    });
};

/**
 * update a Feed
 *
 * @desc  update data of the Feed with the given
 *        id
 *
 * @param {Object} query Query object
 * @param {Object} updates  Update data
 * @param {Function} cb Callback for once update is complete
 */
exports.update = function update(query, updates,  cb) {
  debug('updating Feed: ', query);

  var now = moment().toISOString();
  var opts = {
    'new': true,
    select: returnFields
  };

  Feed
    .findOneAndUpdate(query, updates, opts)
    .populate(population)
    .exec(function updateFeed(err, Feed) {
      if(err) {
        return cb(err);
      }

      cb(null, Feed || {});
    });
};

/**
 * get a Feed.
 *
 * @desc get a Feed with the given id from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.get = function get(query, cb) {
  debug('getting Feed ', query);

  Feed
    .findOne(query, returnFields)
    .populate(population)
    .exec(function(err, Feed) {
      if(err) {
        return cb(err);
      }

      cb(null, Feed || {});
    });
};

/**
 * get a collection of Feeds
 *
 * @desc get a collection of Feeds from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollection = function getCollection(query, qs, cb) {
  debug('fetching a collection of Feeds');

  cb(null,
     Feed
      .find(query, returnFields)
      .populate(population)
      .stream({ transform: JSON.stringify }));

};

/**
 * get a collection of Feeds using pagination
 *
 * @desc get a collection of Feeds from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollectionByPagination = function getCollection(query, qs, cb) {
  debug('fetching a collection of Feeds');

  var opts = {
    fields: returnFields,
    sort: qs.sort ||  { date_created: -1},
    populate: population,
    page: qs.page || 1,
    limit: qs.per_page || 10
  };

  Feed.paginate(query, opts, function (err, data){
    if(err){
      return cb(err);
    }

    var response = {
      page: data.page,
      total_docs: data.total,
      total_pages: data.pages,
      per_page: data.limit,
      docs: data.docs
    };

    cb(null, response);

  });

};
