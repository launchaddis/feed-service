// Access Layer for volunteer Data.

/**
 * Load Module Dependencies.
 */
var debug   = require('debug')('api:dal-volunteer');
var moment  = require('moment');
var _       = require('lodash');

var volunteer    = require('../models/volunteer');
var User     = require('../models/user');

var returnFields = volunteer.whitelist;
var population = [{
  path: 'user',
  select: User.whitelist
}];

/**
 * create a new volunteer.
 *
 * @desc  creates a new volunteer and saves them
 *        in the database
 *
 * @param {Object}  volunteerData  Data for the volunteer to create
 * @param {Function} cb       Callback for once saving is complete
 */
exports.create = function create(volunteerData, cb) {
  debug('creating a new volunteer');

    // Create volunteer if is new.
    var volunteerModel  = new volunteer(volunteerData);

    volunteerModel.save(function savevolunteer(err, data) {
      if (err) {
        return cb(err);
      }


      exports.get({ _id: data._id }, function (err, volunteer) {
        if(err) {
          return cb(err);
        }

        cb(null, volunteer);

      });

    });


};

/**
 * delete a volunteer
 *
 * @desc  delete data of the volunteer with the given
 *        id
 *
 * @param {Object}  query   Query Object
 * @param {Function} cb Callback for once delete is complete
 */
exports.delete = function deleteItem(query, cb) {
  debug('deleting volunteer: ', query);

  volunteer
    .findOne(query, returnFields)
    .populate(population)
    .exec(function deletevolunteer(err, volunteer) {
      if (err) {
        return cb(err);
      }

      if(!volunteer) {
        return cb(null, {});
      }

      volunteer.remove(function(err) {
        if(err) {
          return cb(err);
        }

        cb(null, volunteer);

      });

    });
};

/**
 * update a volunteer
 *
 * @desc  update data of the volunteer with the given
 *        id
 *
 * @param {Object} query Query object
 * @param {Object} updates  Update data
 * @param {Function} cb Callback for once update is complete
 */
exports.update = function update(query, updates,  cb) {
  debug('updating volunteer: ', query);

  var now = moment().toISOString();
  var opts = {
    'new': true,
    select: returnFields
  };

  volunteer
    .findOneAndUpdate(query, data, opts)
    .populate(population)
    .exec(function updatevolunteer(err, volunteer) {
      if(err) {
        return cb(err);
      }

      cb(null, volunteer || {});
    });
};

/**
 * get a volunteer.
 *
 * @desc get a volunteer with the given id from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.get = function get(query, cb) {
  debug('getting volunteer ', query);

  volunteer
    .findOne(query, returnFields)
    .populate(population)
    .exec(function(err, volunteer) {
      if(err) {
        return cb(err);
      }

      cb(null, volunteer || {});
    });
};

/**
 * get a collection of volunteers
 *
 * @desc get a collection of volunteers from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollection = function getCollection(query, qs, cb) {
  debug('fetching a collection of volunteers');

  cb(null,
     volunteer
      .find(query, returnFields)
      .populate(population)
      .stream({ transform: JSON.stringify }));

};

/**
 * get a collection of volunteers using pagination
 *
 * @desc get a collection of volunteers from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollectionByPagination = function getCollection(query, qs, cb) {
  debug('fetching a collection of volunteers');

  var opts = {
    fields: returnFields,
    sort: qs.sort || {},
    populate: population,
    page: qs.page || 1,
    limit: qs.per_page || 10
  };

  volunteer.paginate(query, opts, function (err, data){
    if(err){
      return cb(err);
    }

    var response = {
      page: data.page,
      total_docs: data.total,
      total_pages: data.pages,
      per_page: data.limit,
      docs: data.docs
    };

    cb(null, response);

  });

};
