// Access Layer for Charity Data.

/**
 * Load Module Dependencies.
 */
var debug   = require('debug')('api:dal-Charity');
var moment  = require('moment');
var _       = require('lodash');

var Charity    = require('../models/charity');
var User     = require('../models/user');

var returnFields = Charity.whitelist;
var population = [{
  path: 'user',
  select: User.whitelist
}];

/**
 * create a new Charity.
 *
 * @desc  creates a new Charity and saves them
 *        in the database
 *
 * @param {Object}  CharityData  Data for the Charity to create
 * @param {Function} cb       Callback for once saving is complete
 */
exports.create = function create(CharityData, cb) {
  debug('creating a new Charity');

    // Create Charity if is new.
    var CharityModel  = new Charity(CharityData);

    CharityModel.save(function saveCharity(err, data) {
      if (err) {
        return cb(err);
      }


      exports.get({ _id: data._id }, function (err, Charity) {
        if(err) {
          return cb(err);
        }

        cb(null, Charity);

      });

    });


};

/**
 * delete a Charity
 *
 * @desc  delete data of the Charity with the given
 *        id
 *
 * @param {Object}  query   Query Object
 * @param {Function} cb Callback for once delete is complete
 */
exports.delete = function deleteItem(query, cb) {
  debug('deleting Charity: ', query);

  Charity
    .findOne(query, returnFields)
    .populate(population)
    .exec(function deleteCharity(err, Charity) {
      if (err) {
        return cb(err);
      }

      if(!Charity) {
        return cb(null, {});
      }

      Charity.remove(function(err) {
        if(err) {
          return cb(err);
        }

        cb(null, Charity);

      });

    });
};

/**
 * update a Charity
 *
 * @desc  update data of the Charity with the given
 *        id
 *
 * @param {Object} query Query object
 * @param {Object} updates  Update data
 * @param {Function} cb Callback for once update is complete
 */
exports.update = function update(query, updates,  cb) {
  debug('updating Charity: ', query);

  var now = moment().toISOString();
  var opts = {
    'new': true,
    select: returnFields
  };

  Charity
    .findOneAndUpdate(query, data, opts)
    .populate(population)
    .exec(function updateCharity(err, Charity) {
      if(err) {
        return cb(err);
      }

      cb(null, Charity || {});
    });
};

/**
 * get a Charity.
 *
 * @desc get a Charity with the given id from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.get = function get(query, cb) {
  debug('getting Charity ', query);

  Charity
    .findOne(query, returnFields)
    .populate(population)
    .exec(function(err, Charity) {
      if(err) {
        return cb(err);
      }

      cb(null, Charity || {});
    });
};

/**
 * get a collection of Charitys
 *
 * @desc get a collection of Charitys from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollection = function getCollection(query, qs, cb) {
  debug('fetching a collection of Charitys');

  cb(null,
     Charity
      .find(query, returnFields)
      .populate(population)
      .stream({ transform: JSON.stringify }));

};

/**
 * get a collection of Charitys using pagination
 *
 * @desc get a collection of Charitys from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollectionByPagination = function getCollection(query, qs, cb) {
  debug('fetching a collection of Charitys');

  var opts = {
    fields: returnFields,
    sort: qs.sort || {},
    populate: population,
    page: qs.page || 1,
    limit: qs.per_page || 10
  };

  Charity.paginate(query, opts, function (err, data){
    if(err){
      return cb(err);
    }

    var response = {
      page: data.page,
      total_docs: data.total,
      total_pages: data.pages,
      per_page: data.limit,
      docs: data.docs
    };

    cb(null, response);

  });

};
