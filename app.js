// Load Module Dependencies
// 1. Core Modules
// 2. Third Party Modules
// 3. Local Modules

var express 	= require('express');
var bodyParser 	= require('body-parser');
var mongoose    = require('mongoose');
var debug       = require('debug')('api');
var validator   = require('express-validator');
var cors        = require('cors');

var config 			= require('./config');
var logger			 = require('./lib/logger');
var authenticate     = require('./lib/authenticate');
var router           = require('./routes');

var app = express();

// Connect to MongoDB
mongoose.connect(config.MONGODB_URL);

// Listen to Connection to the DB
mongoose.connection.on('connected', function dbConnection(){
	debug('Connected to the DB');
});

// Listen to Error Connection to the DB
mongoose.connection.on('error', function dbError(){
	debug('Error Connecting to the DB');
});

//Allow CORS
app.use(cors(config.CORS_OPTS));

// Logging HTTP Method and URL
app.use(logger());

app.use(express.static('public'));

// Authentication
app.use(authenticate().unless({
	path: ['/auth/signup', '/auth/login']
}));

// Parser JSON body Requests
app.use(bodyParser.json());

// Express Validator
app.use(validator());

router(app);

// Listen on Port
app.listen(process.env.PORT || config.HTTP_PORT, function connectionListener(){
	console.log('eVolunt NewsFeed Running on Port ', config.HTTP_PORT);
});