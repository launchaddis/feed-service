// Load Module Dependencies

module.exports = {
	// HTTP PORT 
	HTTP_PORT: 7500,	

	// DB Connection URL
	MONGODB_URL: process.env.MONGODB_URL||process.env.DB_URL|| 'mongodb://127.0.0.1:27017/evolunt',

	//TOKEN LENGTH
	TOKEN_LENGTH: 57,

	CORS_OPTS: {
		    origin: '*',
		    methods: 'GET,POST,PUT,DELETE,OPTIONS',
		    allowedHeaders: 'Origin,X-Requested-With,Content-Type,Accept,Authorization'
  	},

  	//Stream Key 
  	STREAM_KEY		: 'pabbggudzxx4',
  	
  	//Stream Secret
  	STREAM_SECRET	: '9y6kf9c36huj4caad7fjepapnddvfzhfp2g8y3j4hnubanvyyqf8nuzx2dyys9ze'
  	

};