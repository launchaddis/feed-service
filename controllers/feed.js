var stream 			= require('getstream');
var EventEmitter 	= require('events').EventEmitter
var moment  		= require('moment');
var debug 			= require('debug')('api:feed-controller');
var _ 				= require('lodash');
	
var volunteerDal	= require('../dal/volunteer');	
var feedDal			= require('../dal/feed');
var charityDal		= require('../dal/charity');
var config 			= require('../config');



var client = stream.connect(config.STREAM_KEY, config.STREAM_SECRET);



exports.postFeedVolunteer = function postFeedVolunteer(req, res, next){
	debug('post volunteer to time line');
	var id = req.params.id;

	var workflow = new EventEmitter();
	var body = req.body;

	workflow.on('getInfo', function () {
		
		volunteerDal.get({_id: id}, function(err, data){
			if(err){
				res.status(500);
					res.json({
						status: 500,
						type: 'volunteer_GET_ERROR',
						message: err.message
					});
						return;
				}

			workflow.emit('createActiviy', data);
		});

	});

	workflow.on('createActiviy', function(data){
	
		var user = client.feed('user', data._id);
		var stamp = new Date().getTime() ;
		var now = moment().toISOString();

		

		var Activity={
			verb: 			'post',
  			volunteer:  	data._id,
			post_message: 	body.post_message
		};
		if(req.file) {
			Activity.image = '/images/' + req.file.filename;
		}

		feedDal.create(Activity, function createCB(err, Feeds){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'CREATE_FEED_ERROR',
					message: err.message
				});
				return;
			}
			workflow.emit('res', Feeds);
		});

	});	

	workflow.on('res',function(data){

			res.json(data);
	});

	workflow.emit('getInfo');
};


exports.postFeedCharity = function postFeedCharity(req, res, next){
	debug('post charity to time line');

	var id = req.params.id;

	var workflow = new EventEmitter();
	var body = req.body;

	workflow.on('getInfo', function () {
		
		charityDal.get({_id: id}, function(err, data){
			if(err){
				res.status(500);
					res.json({
						status: 500,
						type: 'CHARITY_GET_ERROR',
						message: err.message
					});
						return;
				}

			workflow.emit('createActiviy', data);
		});

	});

	workflow.on('createActiviy', function(data){
	
		var user = client.feed('user', data._id);
		var stamp = new Date().getTime() ;
		var now = moment().toISOString();

		var Activity={
			verb: 			'post',
  			charity:  	    data._id,
			post_message: 	body.post_message
		};
		if(req.file) {
			Activity.image = '/images/' + req.file.filename;
		}


		feedDal.create(Activity, function createCB(err, Feeds){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'CREATE_FEED_ERROR',
					message: err.message
				});
				return;
			}
			workflow.emit('res', Feeds);
		});

	})

	workflow.on('res',function(data){

			res.json({
			data: data
		})
	});

	workflow.emit('getInfo');
};



exports.getTimeLineVolunteer = function getTimeLineVolunteer(req, res, next){
	debug('Get Feeds Time line');

	var id = req.params.id;
	var perpage = Number(req.query.limit) || 10;
	var user = client.feed('timeline', id);
	var workflow = new EventEmitter();


	workflow.on('getInfo', function () {
		
		volunteerDal.get({_id: id}, function(err, data){
			if(err){
				res.status(500);
					res.json({
						status: 500,
						type: 'volunteer_GET_ERROR',
						message: err.message
					});
						return;
				}

			workflow.emit('getFeed', data);
		});
	});

	workflow.on('getFeed', function(data){

		var query = { $or:  [{ charity : { $in : data.following } },{ volunteer : { $in : data.following } }] };
		var qs = req.query;

		feedDal.getCollectionByPagination(query, qs, function (err, docs){
			if(err) {
					res.status(500);
					res.json({
						status: 500,
						type: 'FEED_COLLECTION_PAGINATED_ERROR',
						message: err.message
					});
					return;
			}

				res.json(docs);
		})
		
	});	

	workflow.emit('getInfo');
}

exports.getTimeLineCharity = function getTimeLineCharity(req, res, next){
	debug('Get Feeds Time line');

	var id = req.params.id;
	var perpage = Number(req.query.limit) || 10;
	var user = client.feed('timeline', id);
	var workflow = new EventEmitter();


	workflow.on('getInfo', function () {
		
		charityDal.get({_id: id}, function(err, data){
			if(err){
				res.status(500);
					res.json({
						status: 500,
						type: 'CHARITY_GET_ERROR',
						message: err.message
					});
						return;
				}

			workflow.emit('getFeed', data);
		});
	});

	workflow.on('getFeed', function(data){

		var query = { $or:  [{ charity : { $in : data.following } },{ volunteer : { $in : data.following } }] };
		var qs = req.query;

		feedDal.getCollectionByPagination(query, qs, function (err, docs){
			if(err) {
					res.status(500);
					res.json({
						status: 500,
						type: 'FEED_COLLECTION_PAGINATED_ERROR',
						message: err.message
					});
					return;
			}

				res.json(docs);
		})
		
	});	

	workflow.emit('getInfo');
}

exports.getUserFeed = function getUserFeed(req, res, next){
	debug('Get Feeds User');
		var id = req.params.id;
		var perpage = Number(req.query.limit) || 10;
		var query = { $or:  [{ charity : id  },{ volunteer : id }] };
		var qs = req.query;

		feedDal.getCollectionByPagination(query, qs, function (err, docs){
			if(err) {
					res.status(500);
					res.json({
						status: 500,
						type: 'FEED_COLLECTION_PAGINATED_ERROR',
						message: err.message
					});
					return;
			}

				res.json(docs);
		})
		
}



exports.deleteFeed = function deleteFeed(req, res, next){
	debug('delete a feed');

	var _id			= req.params.id;

		feedDal.delete({_id : _id}, function removeCB(err, Event){
		
				if(err){
					res.status(500);
					res.json({
						status: 500,
						type: 'REMOVE_FEED_ERROR',
						message: err.message
					});
					return;
				}
				res.json(Event || {} );
			});
	
}

exports.updateFeed = function updateFeed(req, res, next){
	debug('Update a feed');

	var id 			= req.params.id;
	var body 	 	= req.body;

	if(req.file) {
		body.picture = '/images/' + req.file.filename;
	}

	feedDal.update({ _id: id }, body, function(err, event){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'event_UPDATE_ERROR',
				message: err.message
			});
			return;
		}

		res.json(event);
	})
	
}

exports.like = function like(req, res, next){
	debug('Like a Feed');

	var id 		= req.params.id;
	var body    = req.body;
	var like = {};

	like.id = body.id;
	var workflow = new EventEmitter();

	workflow.on('getFeed', function (){

		feedDal.get({_id : id}, function removeCB(err, Feed){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'GET_FEED_ERROR',
					message: err.message
				});
				return;
			}
			console.log(Feed);
			workflow.emit('already_like', Feed);
		});
	});

	workflow.on('already_like', function(feed){
		if(_.isEmpty(feed)){
			res.json({
				message: "There is no feed by the given ID"
			})
		}

		if(feed.likes.indexOf(like.id) === -1){
			workflow.emit('like',feed);
		}else{
			res.json({
				message: "LIKE_ERROR_ALREADY_LIKED"
			});
		}

	});

	workflow.on('like', function(Feed){
		try{
		Feed.likes.push(like.id);
		}catch(err){
			res.json(err);
			return;
		}
		feedDal.update({ _id: id }, Feed, function(err, event){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'event_UPDATE_ERROR',
					message: err.message
				});
				return;
			}

			res.json(event);
		})
	});
	workflow.emit('getFeed');
}

exports.Unlike = function Unlike(req, res, next){
	debug('Like a Feed');

	var id 		= req.params.id;
	var body    = req.body;
	var like = {};

	like.id = body.id;
	var workflow = new EventEmitter();

	workflow.on('getFeed', function (){

		feedDal.get({_id : id}, function removeCB(err, Feed){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'GET_FEED_ERROR',
					message: err.message
				});
				return;
			}
			workflow.emit('like', Feed);
		});
	});

	workflow.on('like', function(Feed){
		if(_.isEmpty(Feed)){
			res.json({
				message: "There is no feed by the given ID"
			})
		}
		
		var index = Feed.likes.indexOf(like.id);
        Feed.likes.splice(index, 1);

		feedDal.update({ _id: id }, Feed, function(err, event){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'FEED_UPDATE_ERROR',
					message: err.message
				});
				return;
			}

			res.json(event);
		})
	});
	workflow.emit('getFeed');
}

exports.comment = function comment(req, res, next){
	debug('Comment on a Feed');

	var id 		= req.params.id;
	var body    = req.body;
	var Comment = {};
	var now = moment().toISOString();

	Comment.charity   = body.charity;
	Comment.volunteer = body.volunteer;
	Comment.comment   = body.comment;
	Comment.date 	  = now;
	var workflow = new EventEmitter();

	workflow.on('getFeed', function (){

		feedDal.get({_id : id}, function removeCB(err, Feed){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'GET_FEED_ERROR',
					message: err.message
				});
				return;
			}
			workflow.emit('Comment', Feed);
		});
	});

	workflow.on('Comment', function(Feed){
		
		
        Feed.comments.push(Comment);

		feedDal.update({ _id: id }, Feed, function(err, event){
			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'FEED_UPDATE_ERROR',
					message: err.message
				});
				return;
			}

			res.json(event);
		})
	});
	workflow.emit('getFeed');
}

// Noop Method
exports.noop = function noop(req, res, next) {
	res.json({
		message: 'To be Implemented'
	})
}