
/**
 * Load Module Dependencies.
 * education level: High school, undergraduate, graduate, Phd, none
 * experience level: beginner, intermediate, professional
 */
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');

var Schema = mongoose.Schema;
//Event
var EventSchema = new Schema({
  created_by:     { type: Schema.Types.ObjectId, ref: 'User' },
  charity:        { type: Schema.Types.ObjectId, ref: 'Charity' },
  volunteer:      { type: Schema.Types.ObjectId, ref: 'Volunteer' },
  picture:        { type: String, default: '' },
  title:          { type: String },
  description:    { type: String },
  location:       { type: String },
  fund:           { type: Schema.Types.ObjectId, ref: 'Fund' },
  date_of_event:  { type: Date},
  intersetd_users:[{ type: Schema.Types.ObjectId, ref: 'User' }], 
  date_created:   { type: Date },
  last_modified:  { type: Date }
});

// add mongoose-troop middleware to support pagination
EventSchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
EventSchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Event Attributes to expose
 */
EventSchema.statics.whitelist = {

  _id: 1,
  charity:    1,
  volunteer:    1,
  picture:        1,
  title:          1,
  description:    1,
  location:       1,
  fund:           1,
  date_of_event:  1,
  intersetd_users:1, 
  date_created:   1,
  user: 1,
  last_modified: 1
};


/**
 * Event Attributes to expose To Feed
 */
EventSchema.statics.whitelistFeed = {

  _id: 1,
  picture:        1,
  title:          1,
  description:    1,
  location:       1
};

// Expose Event Event
module.exports = mongoose.model('Event', EventSchema);
