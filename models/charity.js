
/**
 * Load Module Dependencies.
 * education level: High school, undergraduate, graduate, Phd, none
 * experience level: beginner, intermediate, professional
 */
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');

var Schema = mongoose.Schema;

var CharitySchema = new Schema({
  user:           { type: Schema.Types.ObjectId, ref: 'User' },
  picture:        { type: String, default: '' },
  name:           { type: String },
  city:           { type: String },
  website:        { type: String },
  phone_number:   { type: Number },
  description:    { type: String },
  causes:         { type: Number },
  location:       [{ type: String }],
  interests:      [{ type: String }],
  following:      [{ type: Schema.Types.ObjectId, ref: 'User' }],
  followers:      [{ type: Schema.Types.ObjectId, ref: 'User' }],
  user_name:      {type: String},       
  date_created:   { type: Date },
  last_modified:  { type: Date }
});

// add mongoose-troop middleware to support pagination
CharitySchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
CharitySchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Charity Attributes to expose
 */
CharitySchema.statics.whitelist = {
  user:           1,
  picture:        1,
  name:           1,
  city:           1,
  website:        1,
  phone_number:   1,
  description:    1,
  rating:         1,
  location:       1,
  causes:         1,
  following:      1,
  followers:      1,
  user_name:      1,       
  date_created:   1,
  last_modified:  1
};

/**
 * Charity Attributes to expose To Feed
 */
CharitySchema.statics.whitelistFeed = {
  _id:            1,
  picture:        1,
  name:           1,
  description:    1
};



// Expose Charity Charity
module.exports = mongoose.model('Charity', CharitySchema);
