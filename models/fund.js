
/**
 * Load Module Dependencies.
 * education level: High school, undergraduate, graduate, Phd, none
 * experience level: beginner, intermediate, professional
 */
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');

var Schema = mongoose.Schema;
//Fund
var FundSchema = new Schema({
  charity:        { type: Schema.Types.ObjectId, ref: 'Charity' },
  volunteer:      { type: Schema.Types.ObjectId, ref: 'Volunteer' },
  picture:          { type: String, default: '' },
  title:            { type: String },
  description:      { type: String },
  location:         { type: String },
  _event:           { type: Schema.Types.ObjectId, ref: 'Event' },
  amount:             {type: Number},
  contibuted_users: [{ type: Schema.Types.ObjectId, ref: 'User' }], 
  date_created:     { type: Date },
  last_modified:    { type: Date }
});

// add mongoose-troop middleware to support pagination
FundSchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
FundSchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Fund Attributes to expose
 */
FundSchema.statics.whitelist = {

  _id: 1,
    created_by:    1,
  picture:        1,
  title:          1,
  description:    1,
  location:       1,
  _event:           1,
  amount:  1,
  contibuted_users:1, 
  date_created:   1,
  last_modified: 1
};

/**
 * Fund Attributes to expose
 */
FundSchema.statics.whitelistFeed = {

  _id: 1,
  picture:        1,
  title:          1,
  description:    1,
  location:       1
};


// Expose Fund Fund
module.exports = mongoose.model('Fund', FundSchema);
