
/**
 * Load Module Dependencies.
 * 
 */
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');

var Schema = mongoose.Schema;

var VolunteerSchema = new Schema({
  user:           { type: Schema.Types.ObjectId, ref: 'User' },
  picture:        { type: String, default: '' },
  first_name:     { type: String },
  last_name:      { type: String },
  locations:       [{ type: String }],
  interests:        [{ type: String }],
  following:      [{ type: Schema.Types.ObjectId, ref: 'User' }],
  followers:      [{ type: Schema.Types.ObjectId, ref: 'User' }],
  birth_date:     {type:  Date},
  gender:         {type: String}, 
  user_name:      {type: String},       
  date_created:   { type: Date },
  last_modified:  { type: Date }
});

// add mongoose-troop middleware to support pagination
VolunteerSchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
VolunteerSchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Volunteer Attributes to expose
 */
VolunteerSchema.statics.whitelist = {
  _id:           1,
  picture:        1,
  first_name:     1,
  last_name:      1,
  locations:       1,
  intrest:        1,
  following:      1,
  birth_date:     1,
  gender:         1, 
  user_name:      1,       
  date_created:   1,
  last_modified:  1
};

/**
 * Volunteer Attributes to expose
 */
VolunteerSchema.statics.whitelistFeed = {
  _id:           1,
  picture:        1,
  first_name:     1,
  last_name:      1
};


// Expose Volunteer Volunteer
module.exports = mongoose.model('Volunteer', VolunteerSchema);
