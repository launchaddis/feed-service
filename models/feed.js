
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');
//Feed
var Schema = mongoose.Schema;

var FeedSchema = new Schema({
  charity:        { type: Schema.Types.ObjectId, ref: 'Charity'},
  volunteer:      { type: Schema.Types.ObjectId, ref: 'Volunteer'},
  image:          { type: String},
  post_message:   { type: String},
  fund:           { type: Schema.Types.ObjectId, ref: 'Fund'},
  event:          { type: Schema.Types.ObjectId, ref: 'Event'},
  verb:           { type: String},
  likes:          [ { type: Schema.Types.ObjectId, ref : 'Volunteer'}],
  comments:       [{volunteer: Schema.Types.ObjectId, charity: Schema.Types.ObjectId, comment : String , date: Date}],
  date_created:   { type: Date },
  last_modified:  { type: Date }
});

// add mongoose-troop middleware to support pagination
FeedSchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
FeedSchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Feed Attributes to expose
 */
FeedSchema.statics.whitelist = {
  charity:       1,
  volunteer:      1,
  image:          1,
  post_message:   1,
  fund:           1,
  event:          1,
  verb:          1,
  date_created:   1,
  likes:          1,
  comments:       1,
  last_modified:  1
};


// Expose Feed Feed
module.exports = mongoose.model('Feed', FeedSchema);
